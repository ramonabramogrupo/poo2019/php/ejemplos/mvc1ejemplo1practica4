<?php

namespace clases;

class Numeros {
    public $originales;
    public $ordenados;
    
    public function __construct($valores) {
        $this->originales=$valores;
        $this->setOrdenados();
    }
    
    public function setOrdenados(){
        $this->ordenados=$this->originales;
        sort($this->ordenados);
        return $this;
    }
    
    public function estanOrdenados(){
        return $this->ordenados===$this->originales;
    }
    
    public function __toString() {
        return join(",",$this->ordenados);
    }
    
    public function getOriginales(){
        return join(",",$this->originales);
    }
    
}
